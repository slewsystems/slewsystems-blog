FROM node:12.7.0-alpine

RUN apk add --no-cache git

RUN mkdir -p /app

WORKDIR /app

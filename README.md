# Engineering Blog

_Powered by [Gatsby](https://gatsbyjs.org), forked from [gatsby-casper](https://github.com/scttcper/gatsby-casper) and hosted on [Gitlab Pages](https://about.gitlab.com/product/pages)._

This repo contains the articles and website of [blog.slewsystems.com](https://blog.slewsystems.com).

## Contributing

Reasonable changes for the following will be accepted:

- Accessibility fixes
- Styling improvements
- Navigation and content curration corrections
- Corrections in existing posts
- Improving post detail or narrative
- Maintance and chores (dependency updating, code styling, etc)

For more information on how to make changes [click here](./CONTRIBUTING.md).
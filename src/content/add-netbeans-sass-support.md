---
layout: post
title: Add SASS Preprocessor to Netbeans 7.x
author: Brandon Patram
tags:
  - Web Development
  - Netbeans
  - Sass
  - Scss
image: img/samuel-zeller-oBb-Y26PJgg-unsplash.jpg
date: 2014-01-18T23:46:37.121Z
---

What this post will go over is how to set up SASS/SCSS support for Netbeans 7.x. In this example I am running macOS but the steps should be similar for Windows or Linux (other than file locations).

---

This article was written for Netbeans 7.X and below. As of Netbeans 8.0 and above they have added support for SASS and LESS natively! You do NOT have to follow these steps if you are using Netbeans 8.0 and above.

## What you'll need:

- Netbeans 7.4
- Ruby (this comes preinstalled on macOS)
- Ruby Gems (also preinstalled on macOS)
- SASS Gem

---

## Installing Ruby and the SASS gem:

1. If you have not already please install [Ruby Gems](http://rubygems.org/pages/download). If you are using Windows or Linux you will need to install Ruby first. If you are running Windows I suggest you use [RubyInstaller](http://rubyinstaller.org/).
1. After you install Ruby you'll need to install the SASS gem. To do this, open up Terminal or if on Windows launch “Command prompt with Ruby” which is installed with RubyInstaller.
1. Type in `gem install sass -V` The `-V` flag will allow you to see what it is installing in case it hangs when doing so. After the gem installs it should output "1 gem installed." And that’s it!

## Setting Up SASS in Netbeans

1. Open up Netbeans (in this post I am using Netbeans 7.4 which natively supports SASS)
1. Open up the Preferences (on Mac go to the Netbeans Menu item and then Preferences)
1. Under the Miscellaneous section there will be a “CSS Preprocessors” tab. Go there
1. There will be a a text box for “Sass path”, this needs to point to the sass gem executable. This path will be different depending on what version of SASS and Ruby you have installed.
1. The SASS gem should be located here (on Mac using RVM):

   ```
   /Users/your_username/.rvm/gems/<THE VERSION OF RUBY INSTALLED>/gems/sass-<VERSION OF SASS INSTALLED>/bin/sass
   ```

1. I suggest you uncheck the “Generate extra information (debug)” checkbox as to avoid littering your CSS files with debug lines.
1. Click OK and you’re done! Now all there is to do is use SASS!

## Working with SASS in Netbeans

1. Open up a new project or existing one. Depending on your folder hierarchy you’ll need to have a "`css`" and a "`scss`" folder. These can go where ever you want and you can name them anything you want.
1. Once you make those two folders right click your project and go to Properties.
1. There will be a “CSS Preprocessors” section. Go there!
1. Here you will be able to select the "`scss`" and "`css`" folder. How SASS/SCSS works is you will only work in your scss file and when you save it will output to the css equivalent in the "`css`" folder.

## Other Cool Things You Can Do:

If you want SASS to minify your CSS output you can add `–-style=compressed` under the "Compiler Options" in your project properties window in the "CSS Proprocessors" section.

When you set the output the compressed SASS will strip all comments from the output. If you have various copyright and information comments you can use [loud comments](https://sass-lang.com/documentation/syntax/comments#in-scss) by using `/*! comment here */` rather than just `/* comment here */`

---

## Special thanks and more information:

- http://richwp.com/keep-comment-in-compressed-sass-scss/
- https://sass-lang.com/documentation/cli/dart-sass#style

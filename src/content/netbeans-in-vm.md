---
layout: post
title: Installing Netbeans in a VM
author: Brandon Patram
tags:
  - Developer Environments
  - Netbeans
image: img/davisco-5E5N49RWtbA-unsplash.jpg
date: 2014-02-23T23:46:37.121Z
---

When using VMware you are allowed the option to share your folders with the host OS. Having a Windows VM will set the user's home directory to "`\\vmware-host\Shared Folders`" which allows the guest and host OS to share the documents, downloads, and desktop. This is a great feature but can sometimes cause issues when installing certain applications.

In my instance when trying to install Netbeans on my Windows VM I would receive an error: "Cannot create local directory: \\vmware-host\Shared Folders\.nbi" Fortunately for us, the Netbeans installer accepts some command-line arguments. We need to set the NBI Working Directory to something other than the default home directory. To do this we simply need to execute the Netbeans installer using the `–-userdir` argument.

```bash
<PATH TO NETBEANS INSTALLER EXE> --userdir "C:\Users\<YOUR USERNAME>"
```

_If your folder in the Users folder has a space in it then make sure to put quotes around the entire file path to avoid errors._

Another tip/trick to quickly open up a new command prompt window and `cd` into a directory is to shift+right-click on the folder and click "Open command window here" from the context menu.

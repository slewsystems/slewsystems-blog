---
layout: post
title: Connecting to a Vagrant Box using SSH for FTP and MySQL
author: Brandon Patram
date: 2014-01-01T10:00:00.000Z
image: img/ryan-quintal-W4JT4MOhbRw-unsplash.jpg
tags:
  - Developer Environments
  - Web Development
  - Vagrant
  - PuPHPet
---

What this post will go over is how to connect to a FTP and MySQL server using a Vagrant environment provided by a PuPHPet VM.

---

## What you'll need:

- Vagrant installed
- A PuPHPet local VM set up (in this example I run CentOS 6.4)
- a MySQL database management software (in this example I use MySQL Workbench)
- a FTP client (in this example I use FileZilla)

---

Depending on your PuPHPet configuration your local VM IP address and MySQL username/passwords may be different.
Connecting to FTP using FileZilla:

- Host: _Local VM IP Address (set in PuPHPet)_
- Protocol: `SFTP`
- Logon Type: `Normal`
- User: `vagrant` _(the default username)_
- Password: `vagrant` _(the default password)_
- Default remote directory: `/var/www/html`

## Connecting to MySQL using MySQL Workbench:

- Connection Method: Standard TCP/IP over SSH
- SSH Hostname: _Local VM IP Address (set in PuPHPet)_
- SSH Username: `vagrant` _(the default username)_
- SSH Password: `vagrant` _(the default password)_
- MySQL Hostname: `127.0.0.1`
- MySQL Server Port: `3306` _(default MySQL port)_
- Username:`root`
- Password: _MySQL Root Password (set in PuPHPet)_

By default the MySQL Hostname is set to `127.0.0.1`, however if you need to find this address in case it is changed in your setup you can SSH into your Vagrant VM and use the following command:

- `cat /etc/my.cnf | grep bind-address`
- If the above file does not exist try:
- `cat /etc/mysql/my.cnf | grep bind-address`

---

## Special thanks and more information:

- http://scotch.io/tutorials/get-vagrant-up-and-running-in-no-time
- https://coderwall.com/p/yzwqvg
- http://www.centos.org/docs/5/html/5.2/Deployment_Guide/s1-server-ports.html

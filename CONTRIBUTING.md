# Contributing

Reasonable changes for the following will be accepted:

- Accessibility fixes
- Styling improvements
- Navigation and content curration corrections
- Corrections in existing posts
- Improving post detail or narrative
- Maintance and chores (dependency updating, code styling, etc)

## Submitting a PR

1. Clone/Fork this repo

   ```bash
   git clone git@gitlab.com:slewsystems/slewsystems-blog.git
   ```

1. Install dependencies

   ```bash
   # (optional) Install Node via nodenv
   nodenv install

   yarn install

   # or via dip
   dip provision
   ```

1. Start development server

   ```bash
   # We support Node 12.7.0 compatible runtimes
   yarn dev:start

   # or via dip
   dip compose up
   ```

   ```bash
   # Once running you can access locally at:
   open http://localhost:8000
   ```

   _If you are looking to building the static site run `yarn build` (or `dip yarn build`) instead._

1. Submit a PR after making changes

   ```bash
   # Ensure you run linter/checks for corrections
   yarn lint

   # or via dip
   dip yarn lint
   ```
